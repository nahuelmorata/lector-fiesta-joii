import 'package:flutter/material.dart';
import "package:flutter_barcode_scanner/flutter_barcode_scanner.dart";
import 'package:lector_joii_v2/api/inscriptos.api.dart';

import 'api/actos.api.dart';
import 'api/conferencias.api.dart';
import 'api/muestras.api.dart';
import 'api/seminarios.api.dart';
import 'api/visitas.api.dart';
import 'clases/entidades/evento.dart';
import 'clases/enums/evento_tipo.enum.dart';
import 'clases/excepciones/http_exception.dart';
import 'dtos/control_inscripto.dto.dart';
import 'lectores/campos.enum.dart';
import 'lectores/lectores.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MainState();
}

class _MainState extends State<MainPage> {
  String _mensaje = "";
  int _dni = 0;
  List<Evento> _eventos = List.empty();
  Evento? _eventoSeleccionado;

  final TextEditingController _controllerNombres = TextEditingController();
  final TextEditingController _controllerApellidos = TextEditingController();
  final TextEditingController _controllerDni = TextEditingController();

  @override
  initState() {
    super.initState();
    _cargarEventos();
  }

  _cargarEventos() async {
    final eventos = List<Evento>.empty(growable: true);
    try {
      eventos.addAll(await ActosApi.obtenerActos());
      eventos.addAll(await ConferenciasApi.obtenerConferencias());
      eventos.addAll(await MuestrasApi.obtenerMuestras());
      eventos.addAll(await SeminariosApi.obtenerSeminarios());
      eventos.addAll(await VisitasApi.obtenerVisitas());
      eventos.add(Evento(1, "Fiesta", EventoTipoEnum.fiesta));
    } on HttpException catch (ex) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(ex.razon)));
    }
    setState(() {
      _eventos = eventos;
    });
  }

  _limpiar() {
    setState(() {
      _controllerApellidos.text = "";
      _controllerNombres.text = "";
      _controllerDni.text = "";
    });
  }

  _seleccionarEvento(Evento? evento) {
    setState(() {
      _eventoSeleccionado = evento;
    });
  }

  _enviar() async {
    if (_eventoSeleccionado == null) {
      setState(() {
        _mensaje = "Debe seleccionar un evento.";
      });

      return;
    }
    if (_dni == 0) {
      setState(() {
        _mensaje = "El DNI no puede estar vacio.";
      });

      _limpiar();
      return;
    }

    setState(() {
      _mensaje = "Verificando...";
    });

    bool marcoAsistencia = false;
    final controlInscriptoDTO = ControlInscriptoDTO(_dni, _eventoSeleccionado!.id);
    switch (_eventoSeleccionado?.tipo) {
      case EventoTipoEnum.conferencia:
        marcoAsistencia = await ConferenciasApi.marcarAsistencia(controlInscriptoDTO);
        break;
      case EventoTipoEnum.muestra:
        marcoAsistencia = await MuestrasApi.marcarAsistencia(controlInscriptoDTO);
        break;
      case EventoTipoEnum.seminario:
        marcoAsistencia = await SeminariosApi.marcarAsistencia(controlInscriptoDTO);
        break;
      case EventoTipoEnum.visita:
        marcoAsistencia = await VisitasApi.marcarAsistencia(controlInscriptoDTO);
        break;
      case EventoTipoEnum.acto:
        marcoAsistencia = await ActosApi.marcarAsistencia(controlInscriptoDTO);
        break;
      case EventoTipoEnum.fiesta:
        marcoAsistencia = await InscriptosApi.verificarAccesoFiesta(controlInscriptoDTO);
        break;
      case null:
        setState(() {
          _mensaje = "Error al verificar. Intente nuevamente.";
        });
        return;
    }

    setState(() {
      _mensaje = marcoAsistencia ? "Puede pasar" : "No puede pasar";
    });

    _limpiar();
  }

  Future _leerQR() async {
    try {
      String textoCodigo = await FlutterBarcodeScanner.scanBarcode("#ff0000", "Cancelar", true, ScanMode.BARCODE);

      final lector = lectores.firstWhere((lector) => lector.puedeLeer(textoCodigo));

      String nombres = lector.leer(textoCodigo, CamposEnum.nombre);
      String apellidos = lector.leer(textoCodigo, CamposEnum.apellido);
      int dni = int.parse(lector.leer(textoCodigo, CamposEnum.dni));

      setState(() {
        _mensaje = "";
        _dni = dni;
        _controllerApellidos.text = apellidos;
        _controllerNombres.text = nombres;
        _controllerDni.text = dni.toString();
      });
    } on FormatException {
      setState(() {
        _mensaje = "Error al leer el documento o volviste antes de poder leer";
      });
    } catch (ex) {
      setState(() {
        _mensaje = "Error desconocido, vuelva a intentar";
      });
    }
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text("JOII Control"),
        ),
        body: Center(
            child: SizedBox(
          width: MediaQuery.of(context).size.width * 0.8,
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text(
              _mensaje,
              style: const TextStyle(fontSize: 24),
            ),
            DropdownButtonHideUnderline(
              child: DropdownButton(
                  value: _eventoSeleccionado,
                  isExpanded: true,
                  hint: const Text("Seleccione un evento..."),
                  items: _eventos
                      .map((evento) => DropdownMenuItem(
                            child: Text(evento.nombre),
                            value: evento,
                          ))
                      .toList(),
                  onChanged: _seleccionarEvento),
            ),
            TextField(
              style: const TextStyle(fontSize: 18),
              decoration: const InputDecoration(hintText: "DNI"),
              textInputAction: TextInputAction.next,
              controller: _controllerDni,
              onChanged: (String dni) {
                setState(() {
                  _dni = int.parse(dni);
                });
              },
            ),
            TextField(
              style: const TextStyle(fontSize: 18),
              decoration: const InputDecoration(hintText: "Nombres"),
              enabled: false,
              controller: _controllerNombres,
              textInputAction: TextInputAction.next,
            ),
            TextField(
              style: const TextStyle(fontSize: 18),
              decoration: const InputDecoration(hintText: "Apellidos"),
              enabled: false,
              controller: _controllerApellidos,
              textInputAction: TextInputAction.done,
            ),
          ]),
        )),
        floatingActionButton: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <Widget>[
          FloatingActionButton.extended(
              heroTag: "btnLeer",
              onPressed: _leerQR,
              icon: const Icon(Icons.camera_alt),
              label: const Text("Leer codigo")),
          FloatingActionButton.extended(
            heroTag: "btnEnviar",
            onPressed: _enviar,
            icon: const Icon(Icons.send),
            label: const Text("Enviar"),
          ),
        ]),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      );
}
