import 'dto.dart';

class ActoDTO implements DTO {
  final int id;
  final String nombre;
  final String lugar;
  final DateTime fechaHora;

  ActoDTO(this.id, this.nombre, this.lugar, this.fechaHora);

  @override
  Map toJSON() {
    return {};
  }

  factory ActoDTO.desdeJSON(Map<dynamic, dynamic> json) {
    return ActoDTO(json["id"], json["nombre"], json["lugar"], DateTime.parse(json["fechaHora"]));
  }
}
