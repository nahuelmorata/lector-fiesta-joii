import 'dto.dart';

class ControlInscriptoDTO extends DTO {
  int dni;
  int? idEvento;

  ControlInscriptoDTO(this.dni, this.idEvento);

  factory ControlInscriptoDTO.fromJson(Map<String, dynamic> json) {
    return ControlInscriptoDTO(json["dni"], json["idEvento"]);
  }

  @override
  Map toJSON() {
    return {"dni": dni, "idEvento": idEvento};
  }
}
