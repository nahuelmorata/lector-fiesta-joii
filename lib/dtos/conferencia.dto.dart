import 'dto.dart';

class ConferenciaDTO implements DTO {
  final int id;
  final String nombre;
  final DateTime fechaHora;

  ConferenciaDTO(this.id, this.nombre, this.fechaHora);

  @override
  Map toJSON() {
    return {};
  }

  factory ConferenciaDTO.desdeJSON(Map<dynamic, dynamic> json) {
    return ConferenciaDTO(json["id"], json["nombre"], DateTime.parse(json["fechaHora"]));
  }
}
