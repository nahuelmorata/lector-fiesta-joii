import 'dto.dart';

class FechaDTO implements DTO {
  final DateTime fecha;

  FechaDTO(this.fecha);

  @override
  Map toJSON() {
    return {fecha: fecha};
  }

  factory FechaDTO.desdeJSON(Map<dynamic, dynamic> json) {
    return FechaDTO(DateTime.parse(json["fecha"]));
  }
}
