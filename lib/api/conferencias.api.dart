import '../clases/entidades/evento.dart';
import '../clases/enums/evento_tipo.enum.dart';
import '../clases/excepciones/http_exception.dart';
import '../constantes.dart';
import '../dtos/control_inscripto.dto.dart';
import 'base.api.dart';

class ConferenciasApi extends BaseApi {
  static Future<List<Evento>> obtenerConferencias() async {
    final listaConferencias = await BaseApi.get("$url/conferencias");
    return listaConferencias["items"]
        .map<Evento>((conferenciaJson) =>
            Evento(conferenciaJson["id"], "${conferenciaJson["nombre"]}", EventoTipoEnum.conferencia))
        .toList();
  }

  static Future<bool> marcarAsistencia(ControlInscriptoDTO controlInscriptoDTO) async {
    try {
      final asistenciaMarcada = await BaseApi.post("$url/conferencias/marcarAsistencia", controlInscriptoDTO);
      return asistenciaMarcada["ok"];
    } on HttpException {
      return false;
    }
  }
}
