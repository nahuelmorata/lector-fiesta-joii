import '../clases/entidades/evento.dart';
import '../clases/enums/evento_tipo.enum.dart';
import '../clases/excepciones/http_exception.dart';
import '../constantes.dart';
import '../dtos/control_inscripto.dto.dart';
import 'base.api.dart';

class ActosApi extends BaseApi {
  static Future<List<Evento>> obtenerActos() async {
    final listaActos = await BaseApi.get("$url/actos");
    return listaActos["items"]
        .map<Evento>((actoJson) => Evento(actoJson["id"], "${actoJson["nombre"]}", EventoTipoEnum.acto))
        .toList();
  }

  static Future<bool> marcarAsistencia(ControlInscriptoDTO controlInscriptoDTO) async {
    try {
      final asistenciaMarcada = await BaseApi.post("$url/actos/marcarAsistencia", controlInscriptoDTO);
      return asistenciaMarcada["ok"];
    } on HttpException {
      return false;
    }
  }
}
