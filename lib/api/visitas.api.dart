import '../clases/entidades/evento.dart';
import '../clases/enums/evento_tipo.enum.dart';
import '../clases/excepciones/http_exception.dart';
import '../constantes.dart';
import '../dtos/control_inscripto.dto.dart';
import '../extensiones/string.extension.dart';
import 'base.api.dart';

class VisitasApi extends BaseApi {
  static Future<List<Evento>> obtenerVisitas() async {
    final listaVisitas = await BaseApi.get("$url/visitas");
    return listaVisitas["items"]
        .map<Evento>((visitasJson) => Evento(
            visitasJson["id"], "Visita: ${visitasJson["nombre"].toString().capitalize()}", EventoTipoEnum.visita))
        .toList();
  }

  static Future<bool> marcarAsistencia(ControlInscriptoDTO controlInscriptoDTO) async {
    try {
      final asistenciaMarcada = await BaseApi.post("$url/visitas/marcarAsistencia", controlInscriptoDTO);
      return asistenciaMarcada["ok"];
    } on HttpException {
      return false;
    }
  }
}
