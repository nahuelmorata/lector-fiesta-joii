import 'dart:convert';

import 'package:http/http.dart' as http;

import '../clases/excepciones/http_exception.dart';
import '../constantes.dart';
import '../dtos/dto.dart';

class BaseApi {
  static dynamic get(String url, {Map? headers}) async {
    final response = await http.get(Uri.parse(url), headers: await _generarHeaders());
    if (response.statusCode >= 400 && response.statusCode < 500) {
      throw HttpException(response.statusCode, response.reasonPhrase ?? "");
    }
    if (response.statusCode >= 500) {
      throw HttpException(response.statusCode, "Error en el servidor");
    }
    return response.contentLength == 0 ? null : json.decode(response.body);
  }

  static dynamic post(String url, DTO dto, {Map? headers}) async {
    final response = await http.post(Uri.parse(url), headers: await _generarHeaders(), body: jsonEncode(dto.toJSON()));
    if (response.statusCode >= 400 && response.statusCode < 500) {
      throw HttpException(response.statusCode, response.reasonPhrase ?? "");
    }
    if (response.statusCode >= 500) {
      throw HttpException(response.statusCode, "Error en el servidor");
    }
    return response.contentLength == 0 ? null : json.decode(response.body);
  }

  static Future<Map<String, String>> _generarHeaders() async {
    final headers = {"Content-Type": "application/json"};
    headers["Authorization"] = "Bearer $token";
    return headers;
  }
}
