import 'package:intl/intl.dart';

import '../clases/entidades/evento.dart';
import '../clases/enums/evento_tipo.enum.dart';
import '../clases/excepciones/http_exception.dart';
import '../constantes.dart';
import '../dtos/control_inscripto.dto.dart';
import 'base.api.dart';

class MuestrasApi extends BaseApi {
  static Future<List<Evento>> obtenerMuestras() async {
    final listaMuestras = await BaseApi.get("$url/muestras");
    final dateFormat = DateFormat('dd-MM-yyyy hh:mm');
    return listaMuestras["items"]
        .map<Evento>((muestraJson) => Evento(muestraJson["id"],
            "Muestra ${dateFormat.format(DateTime.parse(muestraJson["fechaHora"]))}", EventoTipoEnum.muestra))
        .toList();
  }

  static Future<bool> marcarAsistencia(ControlInscriptoDTO controlInscriptoDTO) async {
    try {
      final asistenciaMarcada = await BaseApi.post("$url/muestras/marcarAsistencia", controlInscriptoDTO);
      return asistenciaMarcada["ok"];
    } on HttpException {
      return false;
    }
  }
}
