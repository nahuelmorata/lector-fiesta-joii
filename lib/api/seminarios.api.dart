import '../clases/entidades/evento.dart';
import '../clases/enums/evento_tipo.enum.dart';
import '../clases/excepciones/http_exception.dart';
import '../constantes.dart';
import '../dtos/control_inscripto.dto.dart';
import '../extensiones/string.extension.dart';
import 'base.api.dart';

class SeminariosApi extends BaseApi {
  static Future<List<Evento>> obtenerSeminarios() async {
    final listaSeminarios = await BaseApi.get("$url/seminarios");
    return listaSeminarios
        .map<Evento>((seminarioJson) => Evento(seminarioJson["id"],
            "Seminario: ${seminarioJson["nombre"].toString().capitalize()}", EventoTipoEnum.seminario))
        .toList();
  }

  static Future<bool> marcarAsistencia(ControlInscriptoDTO controlInscriptoDTO) async {
    try {
      final asistenciaMarcada = await BaseApi.post("$url/seminarios/marcarAsistencia", controlInscriptoDTO);
      return asistenciaMarcada["ok"];
    } on HttpException {
      return false;
    }
  }
}
