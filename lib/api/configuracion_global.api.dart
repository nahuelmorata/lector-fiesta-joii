import '../constantes.dart';
import '../dtos/fecha.dto.dart';
import 'base.api.dart';

class ConfiguracionGlobalApi extends BaseApi {
  static Future<FechaDTO> obtenerFechaDescarga() async {
    final response = await BaseApi.get("$url/configuracionGlobal/fechaDescarga");
    return FechaDTO.desdeJSON(response);
  }
}
