import '../clases/excepciones/http_exception.dart';
import '../constantes.dart';
import '../dtos/control_inscripto.dto.dart';
import 'base.api.dart';

class InscriptosApi extends BaseApi {
  static Future<bool> verificarAccesoFiesta(ControlInscriptoDTO controlInscriptoDTO) async {
    try {
      final asistenciaMarcada = await BaseApi.post("$url/inscriptos/control/fiesta", controlInscriptoDTO);
      return asistenciaMarcada["ok"];
    } on HttpException {
      return false;
    }
  }
}
