import 'package:flutter/material.dart';
import 'package:lector_joii_v2/main_page.dart';

void main() {
  runApp(const JoiiApp());
}

class JoiiApp extends StatelessWidget {
  const JoiiApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const MainPage());
  }
}
