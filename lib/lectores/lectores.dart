import 'implementaciones/credencial_uns_lector.dart';
import 'implementaciones/dni_lector.dart';
import 'implementaciones/licencia_conducir_lector.dart';

final lectores = [DniLector(), LicenciaConducirLector(), CredencialUnsLector()];
