import 'campos.enum.dart';

abstract class Lector {
  bool puedeLeer(String texto);
  String leer(String texto, CamposEnum campos);
}
