import '../campos.enum.dart';
import '../lector.dart';

class LicenciaConducirLector extends Lector {
  @override
  String leer(String texto, CamposEnum campos) {
    final valoresSplit = texto.split("\n");
    switch (campos) {
      case CamposEnum.nombre:
        return valoresSplit[3];
      case CamposEnum.apellido:
        return valoresSplit[4];
      case CamposEnum.dni:
        return valoresSplit[1];
    }
  }

  @override
  bool puedeLeer(String texto) => texto.contains("\n") && texto.split("\n")[1].length >= 8;
}
