import '../campos.enum.dart';
import '../lector.dart';

class CredencialUnsLector extends Lector {
  @override
  String leer(String texto, CamposEnum campos) {
    switch (campos) {
      case CamposEnum.nombre:
        return "";
      case CamposEnum.apellido:
        return "";
      case CamposEnum.dni:
        return texto.replaceAll("DNI", "");
    }
  }

  @override
  bool puedeLeer(String texto) => texto.startsWith("DNI");
}
