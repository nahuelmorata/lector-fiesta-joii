import '../campos.enum.dart';
import '../lector.dart';

class DniLector extends Lector {
  @override
  String leer(String texto, CamposEnum campos) {
    final splitValores = texto.split("@");
    switch (campos) {
      case CamposEnum.nombre:
        return splitValores[2];
      case CamposEnum.apellido:
        return splitValores[1];
      case CamposEnum.dni:
        return splitValores[4];
    }
  }

  @override
  bool puedeLeer(String texto) => texto.contains("@") && texto.split("@")[4].length >= 8;
}
