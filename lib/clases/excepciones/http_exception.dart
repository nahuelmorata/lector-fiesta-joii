class HttpException implements Exception {
  final int codigo;
  final String razon;
  HttpException(this.codigo, this.razon);
}
