import '../enums/evento_tipo.enum.dart';

class Evento {
  EventoTipoEnum tipo;
  String nombre;
  int id;

  Evento(this.id, this.nombre, this.tipo);
}
